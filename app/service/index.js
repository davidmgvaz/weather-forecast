'use strict';

var app = require('angular').module('weatherForecast');

app.service('fetchWeather', require('./fetchWeather'));
