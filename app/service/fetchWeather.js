'use strict';

module.exports = function($http) {
    var query = "" +
        "select * from weather.forecast " +
        "where woeid in (select woeid from geo.places(1) where text=\"%s\") and u='c'";
    var url = "https://query.yahooapis.com/v1/public/yql?format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys&u=c&q=";

    function buildRequest(text) {
      return url + query.replace("%s", text);
    }

    this.fetch = function (text) {
      return $http.get(buildRequest(text), {
        cache: true
      });


    }
};
