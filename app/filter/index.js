'use strict';

var app = require('angular').module('weatherForecast');

app.filter('parseDate', require('./parseDate'));