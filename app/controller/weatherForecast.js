'use strict';

module.exports = function($scope, $log, fetchWeather) {
    $scope.fetchWeather = function (text) {
        fetchWeather.fetch(text)
            .then(function SuccessCallBack(response) {
                $log.debug(response);
                if (response.status != 200) {
                     $scope.error = 'Error Occurred';
                     $log.error(response);
                } else {
                    $scope.response = response;
                    $scope.forecast = response.data.query.results.channel;
                }
            }, function ErrorCallBack(response) {
                $scope.error = 'Http Error Occurred';
                $log.error(response);
            });
    };
    $scope.fetchWeather('Porto');
};