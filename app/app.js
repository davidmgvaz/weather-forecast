'use strict';

require('angular');
require('angular-route');

var app = angular.module('weatherForecast', ['ngRoute']);

app.constant('VERSION', require('../package.json').version);

require('./service');
require('./filter');
require('./directive');
require('./controller');

app.config(require('./routes'));

//app.config('$routeProvider', ['ngRoute'], function($routeProvider) {
//    console.log('aqui');
//    //$routeProvider.when('/forecast', {templateUrl: 'partials/forecast.html', controller: require('./controller/weatherForecast.js')});
//    //$routeProvider.otherwise({redirectTo: '/forecast'});
//});