'use strict';

module.exports = function($routeProvider) {
  $routeProvider
    .when('/', {
      controller: require('./controller/weatherForecast.js'),
      template: require('./partials/weatherForecast.html')
    });
};