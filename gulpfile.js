var gulp = require('gulp'),
    clean = require('gulp-clean'),
    sass = require('gulp-sass'),
    connect = require('gulp-connect'),
    bower = require('gulp-bower'),

// requires browserify and vinyl-source-stream
    browserify = require('browserify'),
    stringify = require('stringify'),
    source = require('vinyl-source-stream');

var bases = {
    app: 'app/',
    dist: 'dist/'
};

var resources = {
    html: ['index.html'],
    libs: [
        'bower_components/jquery/dist/jquery.js',
        'bower_components/bootstrap-sass/assets/javascripts/bootstrap.js'
    ]
};

gulp.task('bower', function () {
    return bower();
});

// Delete the dist directory
gulp.task('clean', function () {
    return gulp.src(bases.dist)
        .pipe(clean());
});

gulp.task('js', ['clean'], function () {
    // Grabs the app.js file
    return browserify(bases.app + 'app.js')
        .transform(stringify, {
            appliesTo: ['.html']
        })
        // bundles it and creates a file called main.js
        .bundle()
        .pipe(source('main.js'))
        // saves it the public/js/ directory
        .pipe(gulp.dest(bases.dist + 'js/'));
});

gulp.task('css', ['clean'], function () {
    return gulp.src('./style/style.sass')
        .pipe(sass({
            includePaths: [
                'bower_components/bootstrap-sass/assets/stylesheets',
                'bower_components/weather-icons/sass',
                'style',
            ],
            errLigToConsole: true
        }))
        .pipe(gulp.dest(bases.dist + 'css'));
});

// Copy all other files to dist directly
gulp.task('copy', ['clean'], function () {
    // Copy html
    gulp.src(resources.html, {cwd: bases.app})
        .pipe(gulp.dest(bases.dist));

    // Copy lib scripts, maintaining the original directory structure
    gulp.src(resources.libs)
        .pipe(gulp.dest(bases.dist + 'js'));

    //copy weather-icons
    gulp.src('bower_components/weather-icons/font/**.*')
        .pipe(gulp.dest(bases.dist + 'font'));
});

gulp.task('watch', function () {
    return gulp.watch(['app/**/*.js', 'app/partials/*.html', 'style/*.sass', 'app/index.html'], ['build']);
});

gulp.task('build', ['clean', 'js', 'css', 'copy']);

gulp.task('connect', function () {
    connect.server({
        root: bases.dist,
        port: 8888
    })
});

gulp.task('default', ['build', 'connect', 'watch']);