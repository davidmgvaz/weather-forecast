# weather forecast

This project is an application of [AngularJS](http://angularjs.org/).

Using Yahoo Wheather API.

## Getting Started

To get you started you can simply clone this project and install the dependencies:

### Prerequisites

You must have node.js and its package manager (npm) installed.
You can get them from [http://nodejs.org/](http://nodejs.org/).

### Clone

```
git clone https://gitlab.com/davidmgvaz/weather-forecast.git
cd weather-forecast
```

### Install Dependencies

We have two kinds of dependencies in this project: tools and angular framework code.  The tools help
us manage and test the application.

* We get the tools we depend upon via `npm`, the [node package manager][npm].
* We get the angular code via `bower`, a [client-side code package manager][bower].

We have preconfigured `npm` to automatically run `bower` so we can simply do:

```
npm install
```

Behind the scenes this will also call `gulp bower` (which call `bower install`). You should find that you have two new
folders in your project.

* `node_modules` - contains the npm modules
* `bower_components` - contains bower components


### Run the Application

We have preconfigured the project with a simple development web server.  The simplest way to start
this server is:

```
gulp
```

Now browse to the app at `http://localhost:8888`.